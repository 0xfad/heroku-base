FROM alpine:3.10

ENV TZ=Europe/Rome

RUN apk --update --no-cache add \
  php7 \
  php7-fpm \
  php7-common \
  php7-ctype \
  php7-curl \
  php7-fileinfo \
  php7-mbstring \
  php7-zip \
  php7-dom \
  php7-json \
  php7-xml \
  php7-phar \
  php7-tokenizer \
  php7-openssl \
  php7-iconv \
  php7-opcache \
  php7-pgsql \
  php7-pdo_pgsql \
  php7-session \
  php7-pdo \
  argon2-libs \
  wget \
  curl \
  git \
  supervisor \
  nginx \
  tzdata \
  openssl \
  && \
  curl -sS https://getcomposer.org/installer | php7 -- --install-dir=/usr/local/bin --filename=composer \
  && rm -rf /var/www && mkdir -p /var/www \
  && cp /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
  && rm -rf /var/cache/apk/* /root/.composer/cache/* \
  && adduser -D dyno dyno

# fix work iconv library with alphine
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

COPY nginx.conf /etc/nginx/nginx.conf
COPY php.ini /etc/php7/php.ini
COPY php-fpm.conf /etc/php7/php-fpm.conf
COPY supervisord.conf /etc/supervisord.conf
COPY entrypoint.sh /entrypoint.sh

RUN \
  chmod a+x /entrypoint.sh && \
  chmod -R a+rwx /etc/nginx && \
  chmod -R a+rwx /etc/php7 && \
  chmod -R a+rwx /var/log && \
  chmod -R a+rwx /var/lib/nginx && \
  chmod -R a+rwx /var/tmp/nginx && \
  chmod -R a+rwx /var/www

WORKDIR /var/www

USER dyno

ENTRYPOINT ["/entrypoint.sh"]
CMD ["supervisord", "-c", "/etc/supervisord.conf"]
