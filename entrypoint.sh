#!/bin/ash

set -e

# ---------------------------------------------------------------------------------------------------- #
# VARIABLES
# ---------------------------------------------------------------------------------------------------- #
CURRENT_USER="$(whoami)"
OPCACHE_ENABLE=1

# ---------------------------------------------------------------------------------------------------- #
# NGINX
# ---------------------------------------------------------------------------------------------------- #
if [ -f /var/www/nginx.conf ]; then
  mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.original
  mv /var/www/nginx.conf /etc/nginx/nginx.conf
fi
if [ -f /etc/nginx/nginx.conf.original ]; then
  mv /etc/nginx/nginx.conf.original /etc/nginx/nginx.conf
fi

sed -i -E "s/__USER__/${CURRENT_USER}/g" /etc/nginx/nginx.conf
sed -i -E "s/__PORT__/${PORT:-8080}/g" /etc/nginx/nginx.conf

mkdir -p /var/tmp/nginx
# ---------------------------------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------------------------------- #
# PHP-FPM
# ---------------------------------------------------------------------------------------------------- #
if [ "$CURRENT_USER" == "root" ]; then
  CURRENT_USER="nginx"
fi

if [ "$APP_ENV:-dev" == "dev" ]; then
  OPCACHE_ENABLE=0
fi

sed -i -E "s/^listen.owner = .*/listen.owner = ${CURRENT_USER}/" /etc/php7/php-fpm.conf
sed -i -E "s/^user = .*/user = ${CURRENT_USER}/" /etc/php7/php-fpm.conf
sed -i -E "s/^group = (.*)/;group = \1/" /etc/php7/php-fpm.conf
sed -i -E "s/^opcache.enable = .*/opcache.enable = ${OPCACHE_ENABLE}/" /etc/php7/php-fpm.conf
# ---------------------------------------------------------------------------------------------------- #

exec "$@"